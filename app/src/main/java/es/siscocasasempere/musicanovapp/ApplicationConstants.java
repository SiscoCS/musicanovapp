package es.siscocasasempere.musicanovapp;

/**
 * Created by Sisco on 12/09/2015.
 */
public interface ApplicationConstants {

    // Php Application URL to store Reg ID created
    static final String APP_SERVER_URL = "http://www.siscocasasempere.es/gcm/gcm.php?shareRegId=true";
    static final String APP_SERVER_URL_SEND = "http://www.siscocasasempere.es/gcm/gcm.php?push=true";
    static final String APP_SERVER_URL_ADD = "http://www.siscocasasempere.es/gcm/gcm.php?event=true";
    static final String APP_SERVER_URL_GET_EVENTS = "http://www.siscocasasempere.es/gcm/gcm.php?getEvents=true";
    static final String APP_SERVER_URL_SET_ASSISTANCE = "http://www.siscocasasempere.es/gcm/gcm.php?setAssistance=true";
    static final String APP_SERVER_URL_GET_ASSISTANCE = "http://www.siscocasasempere.es/gcm/gcm.php?getAssistance=true";
    static final String APP_SERVER_URL_SET_ASSISTANCE_ADMIN = "http://www.siscocasasempere.es/gcm/wsgcm.php";
    static final String APP_SERVER_URL_DELETE_EVENT = "http://www.siscocasasempere.es/gcm/gcm.php?delete=true";;


    // Google Project Number
    //static final String GOOGLE_PROJ_ID = "317708261016";
    static final String GOOGLE_PROJ_ID = "248418287513";

    // Message Key
    static final String MSG_KEY = "m";

}
