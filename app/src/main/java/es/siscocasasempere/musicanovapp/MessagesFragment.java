package es.siscocasasempere.musicanovapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.ListFragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;


import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import es.siscocasasempere.musicanovapp.ControlBBDD;
import es.siscocasasempere.musicanovapp.calendar.Util;

/**
 * Created by Sisco on 16/09/2015.
 */
public class MessagesFragment extends ListFragment {
    String message = null;
    ControlBBDD bbdd;
    EditText messageSend;
    ImageButton buttonSend;
    RequestParams params = new RequestParams();
    ProgressDialog prgDialog;
    ArrayList<Map<String, String>> listMessages;
    SimpleAdapter adapter;
    ListView lv;
    public MessagesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bbdd = new ControlBBDD(getActivity());


        prgDialog = new ProgressDialog(getActivity());
        // Set Progress Dialog Text
        prgDialog.setMessage("Please wait...");
        // Set Cancelable as False
        prgDialog.setCancelable(false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lv =(ListView) getActivity().findViewById(android.R.id.list);
        refreshMessages();

        messageSend = (EditText) getActivity().findViewById(R.id.messageSend);
        buttonSend = (ImageButton) getActivity().findViewById(R.id.buttonSend);
        buttonSend.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                sendMessage(arg0);
            }
        });

        SharedPreferences prefs =  this.getActivity().getSharedPreferences("UserDetails",
                Context.MODE_PRIVATE);
        if( !prefs.getBoolean("user_mode", false))
        {
            messageSend.setVisibility(View.GONE);
            buttonSend.setVisibility(View.GONE);
        }

    }

    public void refreshMessages(){
        String[] from = { "name", "purpose" };
        int[] to = { android.R.id.text1, android.R.id.text2 };
        listMessages = bbdd.getMessages(50);
        adapter = new SimpleAdapter(getActivity(),listMessages,
                android.R.layout.simple_list_item_2,
                from, to);
        setListAdapter(adapter);

        lv.setSelection(adapter.getCount()-1);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_messages, container, false);
        message =getArguments().getString("message");
        // Inflate the layout for this fragment
        return rootView;
    }



    @Override
    public void onDetach() {
        super.onDetach();
    }
    public void sendMessage(View view) {
        final String messageSimple = messageSend.getText().toString();
        if(messageSimple.equals("")){
            return;
        }
        String messageToSend = "Message|" +messageSimple ;
        messageSend.setText("");
        params.put("message", messageToSend);
        final Context applicationContext = getActivity();
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();

        client.post(ApplicationConstants.APP_SERVER_URL_SEND, params,
                new AsyncHttpResponseHandler() {
                    // When the response returned by REST has Http
                    // response code '200'
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                        //public void onSuccess(String response) {

                        // Hide Progress Dialog
                        prgDialog.hide();
                        if (prgDialog != null) {
                            prgDialog.dismiss();
                        }

                        //TODO: Aqui tendremos que refrescar la lisa de mensajes
                        listMessages.add(putData(messageSimple, ""));
                        adapter.notifyDataSetChanged();
                        lv.setSelection(adapter.getCount() - 1);

                    }

                    // When the response returned by REST has Http
                    // response code other than '200' such as '404',
                    // '500' or '403' etc
                    @Override
                    // public void onFailure(int statusCode, Throwable error, String content) {
                    public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                        // Hide Progress Dialog
                        prgDialog.hide();
                        if (prgDialog != null) {
                            prgDialog.dismiss();
                        }
                        // When Http response code is '404'
                        if (statusCode == 404) {
                            Toast.makeText(applicationContext,
                                    "Requested resource not found",
                                    Toast.LENGTH_LONG).show();
                        }
                        // When Http response code is '500'
                        else if (statusCode == 500) {
                            Toast.makeText(applicationContext,
                                    "Something went wrong at server end",
                                    Toast.LENGTH_LONG).show();
                        }
                        // When Http response code other than 404, 500
                        else {
                            Toast.makeText(
                                    applicationContext,
                                    "Unexpected Error occcured! [Most common Error: Device might "
                                            + "not be connected to Internet or remote server is not up and running], check for other errors as well",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
        }

    private HashMap<String, String> putData(String name, String purpose) {
        HashMap<String, String> item = new HashMap<String, String>();
        item.put("name", name);
        item.put("purpose", purpose);
        return item;
    }


    }


