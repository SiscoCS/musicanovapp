package es.siscocasasempere.musicanovapp;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sisco on 19/09/2015.
 */

public class ControlBBDD extends SQLiteOpenHelper   {
    public ControlBBDD(Context context) {
        super(context, "puntuaciones", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Crea tabla eventos
        db.execSQL("CREATE TABLE events ("+
                "_id INTEGER PRIMARY KEY AUTOINCREMENT,"+
                "key INTEGER, title TEXT, location TEXT,description TEXT,  dateStart LONG," +
                "dateEnd LONG, dateCreation LONG, state TEXT )");
        //Crea tabla eventos temporal
        db.execSQL("CREATE TABLE eventsTmp ("+
                "_id INTEGER PRIMARY KEY AUTOINCREMENT,"+
                "key INTEGER, state TEXT )");
        //Crea tabla mensajes
        db.execSQL("CREATE TABLE messages (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "dateSend LONG, hourSend TEXT, textSend TEXT, user TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    public void storeEventTMP(int key, String state){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("INSERT INTO eventsTmp VALUES (null,"+key+",'"+state+"')");
        db.close();
    }
    public void storeEvent(int key, String title, String location, String description,long dateStart,long dateEnd,long dateCreation,String state){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("INSERT INTO events VALUES (null,"+key+",'"+title+"','"+location+"','"+description+"',"+dateStart+","+dateEnd+","+dateCreation+",'"+state+"')");
        db.close();
    }
    public void storeEvent(int key, String title, String location, String description,long dateStart,long dateEnd,long dateCreation){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("INSERT INTO events (key,title,location,description,dateStart,dateEnd,dateCreation) VALUES ("+key+",'"+title+"','"+location+"','"+description+"',"+dateStart+","+dateEnd+","+dateCreation+")");
        db.close();
    }
    public void storeMessage(long dateSend, String hourSend, String textSend, String user){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("INSERT INTO messages VALUES (null," + dateSend + ",'" + hourSend + "','" + textSend + "','" + user + "')");
        db.close();
    }
    public ArrayList<Map<String,String>> getMessages(int quantity){
        ArrayList<Map<String, String>> list = new ArrayList<Map<String, String>>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from (select * from messages order by _id ASC limit " + quantity + ") order by _id ASC;", null);
        while (cursor.moveToNext()){
            //0:id, 1:dateSend,2:hourSend,3:text
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Date resultdate = new Date(cursor.getLong(1));
            list.add(putData(cursor.getString(3),sdf.format(resultdate)));
        }
        cursor.close();
        db.close();

        return list;
    }

    public ArrayList<String> getEventsListDate(String date) throws ParseException {
        ArrayList<String> listEvents = new ArrayList<String>();
        SQLiteDatabase db = getReadableDatabase();
        long dayStart,dayEnd;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date dayStartDate = sdf.parse(date);
        dayStart = dayStartDate.getTime();
        dayEnd = dayStart + (24 * 3600 * 1000);


        Cursor cursor = db.rawQuery("select title,dateStart from events WHERE dateStart > "+dayStart+" and dateStart < "+dayEnd+" ORDER BY dateStart", null);
        while (cursor.moveToNext()){
            SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm");
            Date resultdate = new Date(cursor.getLong(1));

            listEvents.add(sdf2.format(resultdate)+ " - " + cursor.getString(0));
        }
        cursor.close();
        db.close();

        return listEvents;

    }
    public String getEventDateFromKey(int key){
       String dateEvent = null;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select dateStart from events WHERE key = " + key, null);
        while (cursor.moveToNext()){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date resultdate = new Date(cursor.getLong(0));

            dateEvent = sdf.format(resultdate);
        }
        cursor.close();
        db.close();

        return dateEvent;
    }
    public ArrayList<String> getEventsDate(){
        ArrayList<String> listEvents = new ArrayList<String>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select dateStart from events", null);
        while (cursor.moveToNext()){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date resultdate = new Date(cursor.getLong(0));

            listEvents.add(sdf.format(resultdate));
        }
        cursor.close();
        db.close();

        return listEvents;

    }

    private HashMap<String, String> putData(String name, String purpose) {
        HashMap<String, String> item = new HashMap<String, String>();
        item.put("name", name);
        item.put("purpose", purpose);
        return item;
    }
    public void updateEvents(JSONArray eventsList) throws JSONException {
        //Delete events from now to future and store list events

        String keyIndex = "";

        for (int i=0; i<eventsList.length();i++)
        {
            JSONObject item = eventsList.getJSONObject(i);
            String title = item.getString("title");
            String location = item.getString("location");
            String description = item.getString("description");
            String strStart = item.getString("dateStart");
            String strEnd = item.getString("dateEnd");
            String strCreate = item.getString("dateCreation");

            int idEvent = item.getInt("id");
            keyIndex += String.valueOf(idEvent)+",";
            try {
                SQLiteDatabase db = getReadableDatabase();
                Cursor cursor = db.rawQuery("select * from events WHERE key ="+idEvent, null);
                if (cursor.getCount() == 0){
                    Cursor cursor2 = db.rawQuery("select state from eventsTmp WHERE key ="+idEvent, null);
                    if(cursor2.moveToFirst()){
                        storeEvent(idEvent, title, location, description, Utility.convertDateToMillis(strStart), Utility.convertDateToMillis(strEnd), Utility.convertDateToMillis(strCreate),cursor2.getString(0));
                    }else {
                        storeEvent(idEvent, title, location, description, Utility.convertDateToMillis(strStart), Utility.convertDateToMillis(strEnd), Utility.convertDateToMillis(strCreate));
                    }
                }
                db.close();
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
        keyIndex += "0";
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM events where key NOT IN (" + keyIndex + ") and dateStart>" + System.currentTimeMillis() + "");
        db.execSQL("DELETE FROM eventsTmp where key NOT IN (" + keyIndex + ")");
        db.close();


    }

    public EventMusica getEventsDetail(String date,String item) throws ParseException {
        EventMusica evento = new EventMusica();

        SQLiteDatabase db = getReadableDatabase();

        String[] array = item.split("\\-", -1);
        String dateTmp = date+" "+array[0].trim();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date dateStart = sdf.parse(dateTmp);
        long lDateStart = dateStart.getTime();

        String title = array[1].trim();

        Cursor cursor = db.rawQuery("select title,location,description,dateStart,dateEnd,state,key from events WHERE dateStart =" + lDateStart + " and title = '" + title + "'", null);
        if(cursor.moveToFirst()) {
            evento.setTitle(cursor.getString(0));
            evento.setLocation(cursor.getString(1));
            evento.setDescription(cursor.getString(2));
            evento.setDateStart(cursor.getLong(3));
            evento.setDateEnd(cursor.getLong(4));
            evento.setState(cursor.getString(5));
            evento.setKey(cursor.getInt(6));
        }
        db.close();

        return evento;
    }

    public EventMusica getEventsDetail(int key) throws ParseException {
        EventMusica evento = new EventMusica();

        SQLiteDatabase db = getReadableDatabase();


        Cursor cursor = db.rawQuery("select title,location,description,dateStart,dateEnd,state,key from events WHERE key =" + key, null);
        if(cursor.moveToFirst()) {
            evento.setTitle(cursor.getString(0));
            evento.setLocation(cursor.getString(1));
            evento.setDescription(cursor.getString(2));
            evento.setDateStart(cursor.getLong(3));
            evento.setDateEnd(cursor.getLong(4));
            evento.setState(cursor.getString(5));
            evento.setKey(cursor.getInt(6));
        }
        db.close();

        return evento;
    }
    public void setEventState(int key,String state){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("UPDATE events SET state = '"+state+"' where key = " + key);
        db.close();
    }

}
