package es.siscocasasempere.musicanovapp;

/**
 * Created by Sisco on 12/09/2015.
 */
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import es.siscocasasempere.musicanovapp.calendar.Util;

public class GCMNotificationIntentService extends IntentService {
    // Sets an ID for the notification, so it can be updated
    public static final int notifyID = 9001;
    NotificationCompat.Builder builder;
    ControlBBDD bbdd;

    public GCMNotificationIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        bbdd = new ControlBBDD(getBaseContext());

        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
                    .equals(messageType)) {
                sendNotification("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
                    .equals(messageType)) {
                sendNotification("Deleted messages on server: "
                        + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
                    .equals(messageType)) {
                sendNotification(""+extras.get(ApplicationConstants.MSG_KEY));
            }
        }
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    private void sendNotification(String msg) {
        Intent resultIntent = new Intent(this, MainActivity.class);
        resultIntent.putExtra("msg", msg);
        resultIntent.setAction("-");
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,
                resultIntent, PendingIntent.FLAG_ONE_SHOT);
        //Botones
        Intent resultIntentY = new Intent(this, MainActivity.class);
        resultIntentY.putExtra("msg", msg);
        resultIntentY.setAction("Y");
        PendingIntent resultPendingIntentY = PendingIntent.getActivity(this, 0,
                resultIntentY, PendingIntent.FLAG_ONE_SHOT);

        Intent resultIntentM = new Intent(this, MainActivity.class);
        resultIntentM.putExtra("msg", msg);
        resultIntentM.setAction("M");
        PendingIntent resultPendingIntentM = PendingIntent.getActivity(this, 0,
                resultIntentM, PendingIntent.FLAG_ONE_SHOT);

        Intent resultIntentN = new Intent(this, MainActivity.class);
        resultIntentN.putExtra("msg", msg);
        resultIntentN.setAction("N");
        PendingIntent resultPendingIntentN = PendingIntent.getActivity(this, 0,
                resultIntentN, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder mNotifyBuilder;
        NotificationManager mNotificationManager;

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);



        String[] array = msg.split("\\|", -1);
        if(array[0].equals("Message")){
            mNotifyBuilder = new NotificationCompat.Builder(this)
                    .setContentTitle(getString(R.string.new_message))
                    .setContentText(array[1])
                    .setSmallIcon(R.drawable.clave_de_sol);
        }
        else{
            mNotifyBuilder = new NotificationCompat.Builder(this)
                    .setContentTitle(getString(R.string.new_event))
                    .setContentText(array[1])
                    .setSmallIcon(R.drawable.clave_de_sol);
        }



        // Set pending intent
        mNotifyBuilder.setContentIntent(resultPendingIntent);

        // Set Vibrate, Sound and Light
        int defaults = 0;
        defaults = defaults | Notification.DEFAULT_LIGHTS;
        defaults = defaults | Notification.DEFAULT_VIBRATE;
        defaults = defaults | Notification.DEFAULT_SOUND;

        mNotifyBuilder.setDefaults(defaults);

        // Set autocancel
        mNotifyBuilder.setAutoCancel(true);


        String contentText =getString(R.string.pushDescription);
        if(array[0].equals("Message")){
            bbdd.storeMessage(System.currentTimeMillis(), Util.getCurrentHour(),array[1],"");
            contentText = array[1];

        }
        if(array[0].equals("Event")){
            contentText = array[1];
            mNotifyBuilder.addAction(R.drawable.ic_thumb_up,getString(R.string.accept),resultPendingIntentY);
            mNotifyBuilder.addAction(R.drawable.ic_help,getString(R.string.maybe),resultPendingIntentM);
            mNotifyBuilder.addAction(R.drawable.ic_thumb_down,getString(R.string.decline),resultPendingIntentN);
        }
        // Set the content for Notification
        mNotifyBuilder.setContentText(contentText);
        // Post a notification
        mNotificationManager.notify(notifyID, mNotifyBuilder.build());
    }
}
