package es.siscocasasempere.musicanovapp;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import java.util.Calendar;

/**
 * Created by Sisco on 27/09/2015.
 */
public class DialogoSelectorFecha extends DialogFragment {
    private DatePickerDialog.OnDateSetListener escuchador;
    public void setOnDateSetListener(DatePickerDialog.OnDateSetListener escuchador) {
        this.escuchador = escuchador;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar calendario = Calendar.getInstance();
        Bundle args = this.getArguments();
        int anyo = calendario.get(Calendar.YEAR);
        int mes = calendario.get(Calendar.MONTH);
        int dia = calendario.get(Calendar.DAY_OF_MONTH);
        if (args != null) {
            String fecha = args.getString("fecha");
            String[] array = fecha.split("\\/", -1);
            if (array.length > 2){
                dia = Integer.valueOf(array[0]);
                mes = Integer.valueOf(array[1]);
                mes = mes -1;
                anyo = Integer.valueOf(array[2]);
            }
        }
        return  new DatePickerDialog(getActivity(), escuchador, anyo, mes, dia);
    }
}
