package es.siscocasasempere.musicanovapp;

import android.app.Activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TimePicker;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import java.text.ParseException;

/**
 * Created by Sisco on 20/09/2015.
 */
public class NewEventFragment extends Fragment implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    ImageButton buttonAdd;
    RequestParams params = new RequestParams();
    ProgressDialog prgDialog;
    EditText title,location,description,dateStart,timeStart,dateEnd,timeEnd;
    EditText tempET;
    public NewEventFragment () {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prgDialog = new ProgressDialog(getActivity());
        // Set Progress Dialog Text
        prgDialog.setMessage("Please wait...");
        // Set Cancelable as False
        prgDialog.setCancelable(false);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_new_event, container, false);


        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        title =(EditText)getActivity().findViewById(R.id.title);
        location =(EditText)getActivity().findViewById(R.id.location);
        description =(EditText)getActivity().findViewById(R.id.description);
        dateStart =(EditText)getActivity().findViewById(R.id.dateStart);
        timeStart =(EditText)getActivity().findViewById(R.id.timeStart);
        dateEnd =(EditText)getActivity().findViewById(R.id.dateEnd);
        timeEnd =(EditText)getActivity().findViewById(R.id.timeEnd);

        buttonAdd = (ImageButton) getActivity().findViewById(R.id.addEvent);
        buttonAdd.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                try {
                    addEvent(arg0);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });



        timeStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tempET = timeStart;
                cambiarHora();
            }
        });

        timeEnd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                tempET = timeEnd;
                cambiarHora();
            }
        });

        dateStart.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                tempET = dateStart;
                cambiarFecha();
            }
        });
        dateEnd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                tempET = dateEnd;
                cambiarFecha();
            }
        });
    }

    public void cambiarFecha() {
        DialogoSelectorFecha dialogoFecha = new DialogoSelectorFecha();
        dialogoFecha.setOnDateSetListener(this);
        Bundle args = new Bundle();

        args.putString("fecha", tempET.getText().toString());
        dialogoFecha.setArguments(args);
        dialogoFecha.show(getActivity().getSupportFragmentManager(), "selectorFecha");
    }

    @Override
    public void onDateSet(DatePicker view, int anyo, int mes, int dia) {
        String strDia = "00" + String.valueOf(dia);
        String strMes = "00" + String.valueOf(mes+1);
        String strAnyo = "0000" + String.valueOf(anyo);

        tempET.setText(strDia.substring(strDia.length()-2) + "/" + strMes.substring(strMes.length()-2)+ "/" + strAnyo.substring(strAnyo.length()-4));
    }

    public void cambiarHora() {
        DialogoSelectorHora dialogoHora = new DialogoSelectorHora();
        dialogoHora.setOnTimeSetListener(this);
        Bundle args = new Bundle();

        args.putString("horaminuto", tempET.getText().toString());
        dialogoHora.setArguments(args);
        dialogoHora.show(getActivity().getSupportFragmentManager(), "selectorHora");
    }

    @Override
    public void onTimeSet(TimePicker vista, int hora, int minuto) {
        String strHora = "00" + String.valueOf(hora);
        String strMinuto = "00" + String.valueOf(minuto);

        tempET.setText(strHora.substring(strHora.length()-2) +":"+strMinuto.substring(strMinuto.length()-2));
    }

    public void addEvent(View view) throws ParseException {
        params.put("event", "Event");
        params.put("title",title.getText().toString());
        params.put("location", location.getText().toString());
        params.put("description", description.getText().toString());
        params.put("dateStart", Utility.convertDateToSQL(dateStart.getText().toString() + " " + timeStart.getText().toString()));
        params.put("dateEnd", Utility.convertDateToSQL(dateEnd.getText().toString() + " " + timeEnd.getText().toString()));

        final Context applicationContext = getActivity();
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();

        client.post(ApplicationConstants.APP_SERVER_URL_ADD, params,
                new AsyncHttpResponseHandler() {
                    // When the response returned by REST has Http
                    // response code '200'
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                        //public void onSuccess(String response) {

                        // Hide Progress Dialog
                        prgDialog.hide();
                        if (prgDialog != null) {
                            prgDialog.dismiss();
                        }

                        getFragmentManager().popBackStack();
                    }

                    // When the response returned by REST has Http
                    // response code other than '200' such as '404',
                    // '500' or '403' etc
                    @Override
                    // public void onFailure(int statusCode, Throwable error, String content) {
                    public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                        // Hide Progress Dialog
                        prgDialog.hide();
                        if (prgDialog != null) {
                            prgDialog.dismiss();
                        }
                        // When Http response code is '404'
                        if (statusCode == 404) {
                            Toast.makeText(applicationContext,
                                    "Requested resource not found",
                                    Toast.LENGTH_LONG).show();
                        }
                        // When Http response code is '500'
                        else if (statusCode == 500) {
                            Toast.makeText(applicationContext,
                                    "Something went wrong at server end",
                                    Toast.LENGTH_LONG).show();
                        }
                        // When Http response code other than 404, 500
                        else {
                            Toast.makeText(
                                    applicationContext,
                                    "Unexpected Error occcured! [Most common Error: Device might "
                                            + "not be connected to Internet or remote server is not up and running], check for other errors as well",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }



}
