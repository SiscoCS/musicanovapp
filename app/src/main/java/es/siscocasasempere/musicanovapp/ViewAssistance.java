package es.siscocasasempere.musicanovapp;

import android.app.Dialog;
import android.app.ExpandableListActivity;
import android.app.ListFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import es.siscocasasempere.musicanovapp.calendar.Util;

/**
 * Created by Sisco on 22/09/2015.
 */
public class ViewAssistance  extends DialogFragment implements View.OnClickListener{
    int mStackLevel,key;
    ControlBBDD bbdd;
    ProgressDialog prgDialog;
    View rootView;
    ListView  assistanceList;

    AssistanceAdapter adapter;
    ArrayList<String> values;

    RequestParams params = new RequestParams();

    static ViewAssistance newInstance(int mStackLevel, int key) {
        ViewAssistance f = new ViewAssistance();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("mStackLevel", mStackLevel);
        args.putInt("key", key);
        f.setArguments(args);

        return f;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.assistance, container,
                false);
        mStackLevel = getArguments().getInt("mStackLevel");
        key = getArguments().getInt("key");
        bbdd = new ControlBBDD(getActivity());
        getDialog().setTitle(getString(R.string.review_assistance));

        TextView title = (TextView) rootView.findViewById(R.id.titleEvent);
        ImageButton saveAssistance = (ImageButton)rootView.findViewById(R.id.saveAssistance);
        saveAssistance.setOnClickListener(this);

        assistanceList = (ListView) rootView.findViewById(R.id.assistanceList);
         values = new ArrayList<String>();
        /*{ "Android", "iPhone", "WindowsMobile",
                "Blackberry", "WebOS", "Ubuntu", "Windows7", "Max OS X",
                "Linux", "OS/2", "Ubuntu", "Windows7", "Max OS X", "Linux",
                "OS/2", "Ubuntu", "Windows7", "Max OS X", "Linux", "OS/2",
                "Android", "iPhone", "WindowsMobile" };*/

         adapter = new AssistanceAdapter(getActivity(), values);
        assistanceList.setAdapter(adapter);
        try {
            EventMusica event= bbdd.getEventsDetail(key);
            title.setText(event.getTitle());
            loadAssistance();        } catch (ParseException e) {
            e.printStackTrace();
        }


        return rootView;
    }



    public void loadAssistance()  {

        prgDialog = new ProgressDialog(getActivity());
        // Set Progress Dialog Text
        prgDialog.setMessage("Please wait...");
        // Set Cancelable as False
        prgDialog.setCancelable(false);

        params.put("getAssistance", "true");
        params.put("key", key);


        final Context applicationContext = getActivity();
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();

        client.get(ApplicationConstants.APP_SERVER_URL_GET_ASSISTANCE, params,
                new JsonHttpResponseHandler() {
                    // When the response returned by REST has Http
                    // response code '200'
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        // Pull out the first event on the public timeline
                        try {
                            JSONObject firstEvent = (JSONObject) response.get(0);
                            values.clear();
                            for (int i=0;i<response.length();i++)
                            {
                                JSONObject item = response.getJSONObject(i);
                                String user = item.getString("user");
                                String state = item.getString("state");
                                String instrument = item.getString("instrument");
                                String confirmed = item.getString("confirm");

                                values.add(state+"::separator::"+user+"::separator::"+instrument+"::separator::"+confirmed);
                            }
                            adapter.notifyDataSetChanged();
                            //bbdd.updateEvents(response);
                            // Do something with the response
                            // System.out.println(firstEvent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        // Hide Progress Dialog
                        prgDialog.hide();
                        if (prgDialog != null) {
                            prgDialog.dismiss();
                        }


                    }


                });
    }
    @Override
    public void onClick(View v) {
        StringBuilder result = new StringBuilder();
        JSONArray jsonAssistance = new JSONArray();
        for(int i=0;i<adapter.mCheckStates.size();i++)
        {
           // if(adapter.mCheckStates.get(i)){

                //Assist checked
                String tmp = adapter.values.get(i);
                String[] array = tmp.split("\\:\\:separator\\:\\:");
                //State-user-instrument

                JSONObject item = new JSONObject();
                try {
                    if(adapter.mCheckStates.get(i))
                    {
                        item.put("state","Y");
                    }
                    else{
                        item.put("state","N");
                    }
                    item.put("user",array[1]);
                    item.put("instrument",array[2]);
                    jsonAssistance.put(item);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            //}
        }
        //TODO: Store list in Web Service
        try {
            JSONObject envio = new JSONObject();
            envio.put("key",key);
            envio.put("lista",jsonAssistance);
            submit(envio);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        dismiss();
    }

    public void submit(JSONObject jsonAssistance) throws UnsupportedEncodingException {
        prgDialog = new ProgressDialog(getActivity());
        // Set Progress Dialog Text
        prgDialog.setMessage("Please wait...");
        // Set Cancelable as False
        prgDialog.setCancelable(false);

        final Context applicationContext = getActivity();
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        ByteArrayEntity entity = new ByteArrayEntity(jsonAssistance.toString().getBytes("UTF-8"));
        entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        client.post(getContext(), ApplicationConstants.APP_SERVER_URL_SET_ASSISTANCE_ADMIN, entity, "application/json", new JsonHttpResponseHandler() {

            // When the response returned by REST has Http
            // response code '200'
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject obj) {
                prgDialog.hide();
                if (prgDialog != null) {
                    prgDialog.dismiss();
                }

                dismiss();


            }


            // When the response returned by REST has Http
            // response code other than '200' such as '404',
            // '500' or '403' etc
            @Override
            // public void onFailure(int statusCode, Throwable error, String content) {
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                // Hide Progress Dialog
                prgDialog.hide();
                if (prgDialog != null) {
                    prgDialog.dismiss();
                }
                // When Http response code is '404'
                if (statusCode == 404) {
                    Toast.makeText(applicationContext,
                            "Requested resource not found",
                            Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(applicationContext,
                            "Something went wrong at server end",
                            Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(
                            applicationContext,
                            "Unexpected Error occcured! [Most common Error: Device might "
                                    + "not be connected to Internet or remote server is not up and running], check for other errors as well",
                            Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        Window window = getDialog().getWindow();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

    }
}
