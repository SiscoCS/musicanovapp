package es.siscocasasempere.musicanovapp;

/**
 * Created by Sisco on 22/09/2015.
 */
public class EventMusica {
    String title,description,location,state;
    long dateStart,dateEnd;
    int key;
    public EventMusica()
    {

    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public EventMusica (String aTitle,String aLocation,String aDescription,long aDateStart,long aDateEnd,String aState,int aKey)
    {
        this.title = aTitle;
        this.description = aDescription;
        this.location = aLocation;
        this.dateStart = aDateStart;
        this.dateEnd = aDateEnd;
        this.state = aState;
        this.key = aKey;
    }

    public long getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(long dateEnd) {
        this.dateEnd = dateEnd;
    }

    public long getDateStart() {
        return dateStart;
    }

    public void setDateStart(long dateStart) {
        this.dateStart = dateStart;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
