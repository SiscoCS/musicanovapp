package es.siscocasasempere.musicanovapp;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Sisco on 22/09/2015.
 */
public class AssistanceAdapter extends ArrayAdapter<String> implements CompoundButton.OnCheckedChangeListener {
private final Context context;
public final ArrayList<String> values;
    SparseBooleanArray mCheckStates;

public AssistanceAdapter(Context context, ArrayList<String> values) {
        super(context, R.layout.assistance_row, values);
    this.context = context;
    this.values = values;
    mCheckStates = new SparseBooleanArray(values.size());
        }



        @Override
public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.assistance_row, parent, false);

        TextView tvName = (TextView) rowView.findViewById(R.id.name);
        TextView tvInstrument = (TextView) rowView.findViewById(R.id.instrument);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.userChose);
            CheckBox chkAssist = (CheckBox)rowView.findViewById(R.id.chkAssist);
            chkAssist.setTag(position);
            chkAssist.setOnCheckedChangeListener(this);
        String item = values.get(position);
        String[] array = item.split("\\:\\:separator\\:\\:");
        //State-user-instrument-confirmed

        tvName.setText(array[1]);
        tvInstrument.setText(array[2]);
        String state = array[3];
            if(state.equals("Y")){
                setChecked(position,true);
                chkAssist.setChecked(true);
            }
            else{
                setChecked(position,false);
                chkAssist.setChecked(false);
            }

        String s = array[0];
        if (s.equals("Y")) {
        imageView.setImageResource(R.drawable.ic_thumb_up_black);
        }
            if (s.equals("N")) {
        imageView.setImageResource(R.drawable.ic_thumb_down_black);
        }
            if (s.equals("M")) {
                imageView.setImageResource(R.drawable.ic_help_black);
            }

        return rowView;
        }

    public boolean isChecked(int position) {
        return mCheckStates.get(position, false);
    }

    public void setChecked(int position, boolean isChecked) {
        mCheckStates.put(position, isChecked);

    }

    public void toggle(int position) {
        setChecked(position, !isChecked(position));

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView,
                                 boolean isChecked) {

        mCheckStates.put((Integer) buttonView.getTag(), isChecked);

    }
}