package es.siscocasasempere.musicanovapp;




import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.CalendarView.OnDateChangeListener;
import android.widget.ListView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import es.siscocasasempere.musicanovapp.R;
import es.siscocasasempere.musicanovapp.calendar.*;

/**
 * Created by Sisco on 16/09/2015.
 */
public class CalendarFragment extends ListFragment {

    MFCalendarView mf;
    RequestParams params = new RequestParams();
    ProgressDialog prgDialog;
    ControlBBDD bbdd;
    ArrayList<String> eventsOfTheDay;
    ArrayAdapter<String> adapter;
    String dateShown;
    int idEvent;

    int mStackLevel;

    public CalendarFragment() {
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        SharedPreferences prefs =  this.getActivity().getSharedPreferences("UserDetails",
                Context.MODE_PRIVATE);
        if( prefs.getBoolean("user_mode", false))
        {
            inflater.inflate(R.menu.menu_calendar, menu);
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        bbdd = new ControlBBDD(getActivity());

        prgDialog = new ProgressDialog(getActivity());
        // Set Progress Dialog Text
        prgDialog.setMessage("Please wait...");
        // Set Cancelable as False
        prgDialog.setCancelable(false);
        idEvent = 0;


       eventsOfTheDay = new ArrayList<String>();
         adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, eventsOfTheDay);
        setListAdapter(adapter);

        mStackLevel =0;

    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        final String item = (String) eventsOfTheDay.get(position);

        mStackLevel++;
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.

        DialogFragment eventDetail = EventsDetail.newInstance(mStackLevel,item,dateShown);

        eventDetail.show(ft, "dialog");
        Log.e("EVENTO", "Seleccionado " + item);
        //Cargar detalles del evento


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_calendar, container, false);
        String message =getArguments().getString("message");
        String action = getArguments().getString("action");

        if(action != null && !action.equals(".") && message != null && !message.equals("")) {
            String[] array = message.split("\\|", -1);
            idEvent = Integer.parseInt(array[2]);
            bbdd.storeEventTMP(idEvent, action);
        }
        // Inflate the layout for this fragment
        initializeCalendar(rootView);


        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

   public void initializeCalendar(View v){
       mf = (MFCalendarView) v.findViewById(R.id.mFCalendarView);
       mf.setOnCalendarViewListener(new onMFCalendarViewListener() {

           @Override
           public void onDisplayedMonthChanged(int month, int year, String monthStr) {

               StringBuffer bf = new StringBuffer()
                       .append(" month:")
                       .append(month)
                       .append(" year:")
                       .append(year)
                       .append(" monthStr: ")
                       .append(monthStr);

               //Toast.makeText(getActivity().getApplicationContext(), bf.toString(), Toast.LENGTH_SHORT).show();
               eventsOfTheDay.clear();
               adapter.notifyDataSetChanged();
               showEventsOnCalendar();
           }

           @Override
           public void onDateChanged(String date) {
/*
               Toast.makeText(getActivity().getApplicationContext(), "onDateChanged:" + date,
                       Toast.LENGTH_SHORT).show();*/
               try {
                   showEvents(date);
               } catch (ParseException e) {
                   e.printStackTrace();
               }
           }
       });

       loadEvents();
       try {
           showEvents(Util.getCurrentDate());
       } catch (ParseException e) {
           e.printStackTrace();
       }


   }
    public void showEventsOnCalendar(){


        ArrayList<String> eventDays = bbdd.getEventsDate();
        mf.setEvents(eventDays);

        if(idEvent != 0 && bbdd.getEventDateFromKey(idEvent) != null){
            mf.setDate(bbdd.getEventDateFromKey(idEvent));
        }
    }

    public void showEvents(String date) throws ParseException {
        //Show list of events from picked date
        eventsOfTheDay.clear();
        ArrayList<String> listEventsDate = bbdd.getEventsListDate(date);
        for(String item : listEventsDate){
            eventsOfTheDay.add(item);
        }
        adapter.notifyDataSetChanged();
        Log.e("", "Mostrar eventos del dia" + date);
        dateShown = date;


    }

    public void loadEvents()  {
        params.put("getEvents", "true");
        params.put("date",Util.getCurrentDate());


        final Context applicationContext = getActivity();
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();

        client.get(ApplicationConstants.APP_SERVER_URL_GET_EVENTS, params,
                new JsonHttpResponseHandler() {
                    // When the response returned by REST has Http
                    // response code '200'
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        // Pull out the first event on the public timeline
                        try {
                           // JSONObject firstEvent = (JSONObject) response.get(0);
                            bbdd.updateEvents(response);
                            // Do something with the response
                           // System.out.println(firstEvent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        // Hide Progress Dialog
                        prgDialog.hide();
                        if (prgDialog != null) {
                            prgDialog.dismiss();
                        }
                        showEventsOnCalendar();

                    }


                });
    }
}
