package es.siscocasasempere.musicanovapp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Sisco on 21/09/2015.
 */
public class EventsDetail extends DialogFragment implements View.OnClickListener {
    ControlBBDD bbdd;
    EventMusica event;
    int mStackLevel;

    public static final String REG_ID = "regId";

    RequestParams params = new RequestParams();
    ProgressDialog prgDialog;

    RadioButton rbYes,rbMaybe,rbNo;

    static EventsDetail newInstance(int mStackLevel, String item,String date) {
        EventsDetail f = new EventsDetail();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("mStackLevel", mStackLevel);
        args.putString("item", item);
        args.putString("date", date);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onResume() {
        super.onResume();

        Window window = getDialog().getWindow();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.eventsdetailfragment, container,
                false);

        bbdd = new ControlBBDD(getActivity());
        mStackLevel = getArguments().getInt("mStackLevel");
        String item = getArguments().getString("item");
        String date = getArguments().getString("date");
        getDialog().setTitle(getString(R.string.event_detail));

        ImageButton btn = (ImageButton) rootView.findViewById(R.id.submit);
        btn.setOnClickListener(this);

        ImageButton btnAssistance = (ImageButton) rootView.findViewById(R.id.viewAssistance);
        btnAssistance.setOnClickListener(this);

        ImageButton btnDelete = (ImageButton) rootView.findViewById(R.id.deleteEvent);
        btnDelete.setOnClickListener(this);

        SharedPreferences prefs =  this.getActivity().getSharedPreferences("UserDetails",
                Context.MODE_PRIVATE);
        if( !prefs.getBoolean("user_mode", false))
        {
            btnAssistance.setVisibility(View.GONE);
            btnDelete.setVisibility(View.GONE);
        }

        ImageButton btnAlert = (ImageButton) rootView.findViewById(R.id.addAlert);
        btnAlert.setOnClickListener(this);



        EditText title = (EditText) rootView.findViewById(R.id.title);
        EditText location = (EditText) rootView.findViewById(R.id.location);
        EditText description = (EditText) rootView.findViewById(R.id.description);
        EditText dateStart = (EditText) rootView.findViewById(R.id.dateStart);
        EditText timeStart = (EditText) rootView.findViewById(R.id.timeStart);
        EditText dateEnd = (EditText) rootView.findViewById(R.id.dateEnd);
        EditText timeEnd = (EditText) rootView.findViewById(R.id.timeEnd);

        dateStart.setKeyListener(null);
        timeStart.setKeyListener(null);
        dateEnd.setKeyListener(null);
        timeEnd.setKeyListener(null);

         rbYes = (RadioButton) rootView.findViewById(R.id.RBaccept);
         rbMaybe = (RadioButton) rootView.findViewById(R.id.RBmaybe);
         rbNo = (RadioButton) rootView.findViewById(R.id.RBdecline);
        try {

            event = bbdd.getEventsDetail(date,item);
            title.setText(event.getTitle());
            location.setText(event.getLocation());
            description.setText(event.getDescription());

            long ldateStart = event.getDateStart();
            Date currentDate = new Date(ldateStart);
            SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");

            dateStart.setText(formatDate.format(currentDate));
            timeStart.setText(formatTime.format(currentDate));


            long ldateEnd = event.getDateEnd();
            currentDate = new Date(ldateEnd);
            dateEnd.setText(formatDate.format(currentDate));
            timeEnd.setText(formatTime.format(currentDate));

            String state = event.getState();
            rbYes.setChecked(false);
            rbMaybe.setChecked(false);
            rbNo.setChecked(false);
            if(state != null){
                if(state.equals("Y")){
                    rbYes.setChecked(true);
                }
                if(state.equals("M")){
                    rbMaybe.setChecked(true);
                }
                if(state.equals("N")){
                    rbNo.setChecked(true);
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        // Do something else

        return rootView;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.submit) {
            submit(v);
        }
        if(v.getId() == R.id.deleteEvent){
            deleteEvent();
        }
        if(v.getId() == R.id.addAlert){
            Intent intent = new Intent(Intent.ACTION_INSERT);
            intent.setType("vnd.android.cursor.item/event");
            intent.putExtra(CalendarContract.Events.TITLE, event.getTitle());
            intent.putExtra(CalendarContract.Events.EVENT_LOCATION, event.getLocation());
            intent.putExtra(CalendarContract.Events.DESCRIPTION, event.getDescription());
            intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
                    event.getDateStart());
            intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,
                    event.getDateEnd());
            startActivity(intent);
        }
        if(v.getId() == R.id.viewAssistance) {
           //TODO: Ver asistencias confirmadas
            mStackLevel++;
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            Fragment prev = getFragmentManager().findFragmentByTag("dialog");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);

            // Create and show the dialog.

            DialogFragment assistanceDialog = ViewAssistance.newInstance(mStackLevel,event.getKey());
            assistanceDialog.show(ft, "dialogAssistance");
        }
    }
        public void deleteEvent(){
            prgDialog = new ProgressDialog(getActivity());
            // Set Progress Dialog Text
            prgDialog.setMessage("Please wait...");
            // Set Cancelable as False
            prgDialog.setCancelable(false);

            event.getKey();


            params.put("key", event.getKey());
            final Context applicationContext = getActivity();
            // Make RESTful webservice call using AsyncHttpClient object
            AsyncHttpClient client = new AsyncHttpClient();

            client.post(ApplicationConstants.APP_SERVER_URL_DELETE_EVENT, params,
                    new AsyncHttpResponseHandler() {
                        // When the response returned by REST has Http
                        // response code '200'
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                            //public void onSuccess(String response) {

                            // Hide Progress Dialog
                            prgDialog.hide();
                            if (prgDialog != null) {
                                prgDialog.dismiss();
                            }

                            dismiss();

                        }

                        // When the response returned by REST has Http
                        // response code other than '200' such as '404',
                        // '500' or '403' etc
                        @Override
                        // public void onFailure(int statusCode, Throwable error, String content) {
                        public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                            // Hide Progress Dialog
                            prgDialog.hide();
                            if (prgDialog != null) {
                                prgDialog.dismiss();
                            }
                            // When Http response code is '404'
                            if (statusCode == 404) {
                                Toast.makeText(applicationContext,
                                        "Requested resource not found",
                                        Toast.LENGTH_LONG).show();
                            }
                            // When Http response code is '500'
                            else if (statusCode == 500) {
                                Toast.makeText(applicationContext,
                                        "Something went wrong at server end",
                                        Toast.LENGTH_LONG).show();
                            }
                            // When Http response code other than 404, 500
                            else {
                                Toast.makeText(
                                        applicationContext,
                                        "Unexpected Error occcured! [Most common Error: Device might "
                                                + "not be connected to Internet or remote server is not up and running], check for other errors as well",
                                        Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }
        public void submit(View v){
            prgDialog = new ProgressDialog(getActivity());
            // Set Progress Dialog Text
            prgDialog.setMessage("Please wait...");
            // Set Cancelable as False
            prgDialog.setCancelable(false);

            String state = "-";
            if (rbYes.isChecked()){
                state="Y";
            }
            if (rbMaybe.isChecked()){
                state="M";
            }
            if (rbNo.isChecked()){
                state="N";
            }
            bbdd.setEventState(event.getKey(),state);

            SharedPreferences prefs = getActivity().getSharedPreferences("UserDetails",
                    Context.MODE_PRIVATE);
            String registrationId = prefs.getString(REG_ID, "");

            params.put("key", event.getKey());
            params.put("user", registrationId);
            params.put("state", state);

            final Context applicationContext = getActivity();
            // Make RESTful webservice call using AsyncHttpClient object
            AsyncHttpClient client = new AsyncHttpClient();

            client.post(ApplicationConstants.APP_SERVER_URL_SET_ASSISTANCE, params,
                    new AsyncHttpResponseHandler() {
                        // When the response returned by REST has Http
                        // response code '200'
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                            //public void onSuccess(String response) {

                            // Hide Progress Dialog
                            prgDialog.hide();
                            if (prgDialog != null) {
                                prgDialog.dismiss();
                            }

                            dismiss();
                        }

                        // When the response returned by REST has Http
                        // response code other than '200' such as '404',
                        // '500' or '403' etc
                        @Override
                        // public void onFailure(int statusCode, Throwable error, String content) {
                        public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                            // Hide Progress Dialog
                            prgDialog.hide();
                            if (prgDialog != null) {
                                prgDialog.dismiss();
                            }
                            // When Http response code is '404'
                            if (statusCode == 404) {
                                Toast.makeText(applicationContext,
                                        "Requested resource not found",
                                        Toast.LENGTH_LONG).show();
                            }
                            // When Http response code is '500'
                            else if (statusCode == 500) {
                                Toast.makeText(applicationContext,
                                        "Something went wrong at server end",
                                        Toast.LENGTH_LONG).show();
                            }
                            // When Http response code other than 404, 500
                            else {
                                Toast.makeText(
                                        applicationContext,
                                        "Unexpected Error occcured! [Most common Error: Device might "
                                                + "not be connected to Internet or remote server is not up and running], check for other errors as well",
                                        Toast.LENGTH_LONG).show();
                            }
                        }
                    });

        }


}
