package es.siscocasasempere.musicanovapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;

public class SettingsActivity extends AppCompatActivity {

    SharedPreferences prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

         prefs = getSharedPreferences("UserDetails",
                Context.MODE_PRIVATE);

        Switch switchAdmin = (Switch) findViewById(R.id.switch1);
        switchAdmin.setChecked(prefs.getBoolean("user_mode", false));


        switchAdmin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){

                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putBoolean("user_mode", true);
                    editor.commit();

                }else{
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putBoolean("user_mode", false);
                    editor.commit();
                }
            }
        });
    }


}
